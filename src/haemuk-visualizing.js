'use strict'

var HaemukVisualizing = {
    radar: function (selector, source, config) {
        function sourceToDataset(source) {
            var itemCount = source.axes.length;
            var dataset = [];
            for (var i = 0; i <= itemCount; i++) {
                var data = [{
                    className: source.className,
                    axes: function () {
                        var arr = [];
                        for (var j = 0; j < itemCount; j++) {
                            arr.push({
                                axis: source.axes[j].axis,
                                value: j < i ? source.axes[j].value : 0
                            });
                        }
                        return arr;
                    }()
                }];
                dataset.push(data);
            }
            return dataset;
        }

        var dataset = sourceToDataset(source);

        RadarChart.defaultConfig.color = config.color || function() {};
        RadarChart.defaultConfig.levels = config.levels || 0;
        RadarChart.defaultConfig.radius = config.radius || 0;
        RadarChart.defaultConfig.circles = config.circles || false;
        RadarChart.defaultConfig.w = config.w || 240;
        RadarChart.defaultConfig.h = config.h || 240;
        RadarChart.defaultConfig.maxValue = config.maxValue || 100;
        RadarChart.defaultConfig.minValue = config.minValue || 0;
        RadarChart.defaultConfig.factor = config.factor || 0.8;
        RadarChart.defaultConfig.factorLegend = config.factorLegend || 1;
        RadarChart.defaultConfig.axisLine = config.axisLine || false;

        var chart = RadarChart.chart();
        var cfg = chart.config(); // retrieve default config
        var svg = d3.select(selector).append('svg')
            .attr('width', cfg.w)
            .attr('height', cfg.h);
        svg.append('g').classed('single', 1).datum(dataset[0]).call(chart);

        var i = 0;

        function animatedDraw() {
            svg.select('g.single').datum(dataset[i]).call(chart);
            if (i < source.axes.length) i++;
            setTimeout(animatedDraw, 250);
        }

        animatedDraw();
    },
    bars : function(selector, dataset, config){

        config.w = config.w || 300;
        config.maxValue = config.maxValue || 100;
        config.barThickness = config.barThickness || 20;
        config.barMargin = config.barMargin || 2;
        config.delay = config.delay || 100;
        config.duration = config.duration || 200;
        // Default configuration
        config.labelWidth = 80;
        config.gridWidth = config.w - config.labelWidth;
        config.gridHeight = (config.barThickness + config.barMargin) * dataset.length + config.barThickness;

        var svg = d3.select(selector)
            .append('svg')
            .attr('width', config.w)
            .attr('height', config.gridHeight + 30);

        svg.selectAll('rect')
            .data(function() {
                var data = [];
                for(var i=0; i<dataset.length; i++) {
                    data.push(dataset[i].value);
                }
                return data;
            })
            .enter()
            .append('rect')
            .attr('x', config.labelWidth)
            .attr('y', function(d, i) { return config.barMargin/2 + i * (config.barThickness + config.barMargin); })
            .attr('width', 0)
            .attr('height', config.barThickness)
            .attr('fill', '#fff')
            .transition()
            .delay(function(d, i) { return i * config.delay; })
            .duration(config.duration)
            .attr('width', function(d) { return d * (config.gridWidth - config.barMargin) / config.maxValue; })
            .attr('fill', function(d, i) {
                if(dataset[i].value > config.maxValue*2/3) {
                    return '#ff4e71';
                } else if(dataset[i].value <= config.maxValue*2/3 && dataset[i].value > config.maxValue*1/3) {
                    return '#fab065';
                } else {
                    return '#8ae6b3';
                }
            });

        svg.selectAll('text')
            .data(function() {
                var data = [];
                for(var i=0; i<dataset.length; i++) {
                    data.push(dataset[i].value);
                }
                return data;
            })
            .enter()
            .append('text')
            .text(function(d, i) { return dataset[i].axis; })
            .attr('x', 8)
            .attr('y', function(d, i) { return (config.barThickness + config.barMargin) * (i+1) - (config.barThickness/2); })
            .attr('font-size', 12);

        // 가로선
        svg.append('line')
            .attr('x1', 0)
            .attr('x2', config.w)
            .attr('y1', (config.barThickness + config.barMargin) * dataset.length + config.barThickness)
            .attr('y2', (config.barThickness + config.barMargin) * dataset.length + config.barThickness)
            .attr('shape-rendering', 'crispEdges')
            .attr('stroke-width', 1)
            .attr('stroke', '#dbdbdb');

        // 세로선 0
        svg.append('line')
            .attr('x1', config.labelWidth)
            .attr('x2', config.labelWidth)
            .attr('y1', 0)
            .attr('y2', (config.barThickness + config.barMargin) * dataset.length + config.barThickness)
            .attr('shape-rendering', 'crispEdges')
            .attr('stroke-width', 1)
            .attr('stroke', '#8a8a8a')
            .attr('stroke-dasharray', ('1, 2'));

        // 세로선 50
        svg.append('line')
            .attr('x1', config.labelWidth + (config.gridWidth - config.barMargin) / 2)
            .attr('x2', config.labelWidth + (config.gridWidth - config.barMargin) / 2)
            .attr('y1', 0)
            .attr('y2', (config.barThickness + config.barMargin) * dataset.length + config.barThickness)
            .attr('shape-rendering', 'crispEdges')
            .attr('stroke-width', 1)
            .attr('stroke', '#8a8a8a')
            .attr('stroke-dasharray', ('1, 2'));

        // 세로선 100
        svg.append('line')
            .attr('x1', config.w - config.barMargin)
            .attr('x2', config.w - config.barMargin)
            .attr('y1', 0)
            .attr('y2', (config.barThickness + config.barMargin) * dataset.length + config.barThickness)
            .attr('shape-rendering', 'crispEdges')
            .attr('stroke-width', 1)
            .attr('stroke', '#8a8a8a')
            .attr('stroke-dasharray', ('1, 2'));

        svg.append('text')
            .text('양호')
            .attr('y', config.gridHeight + 19)
            .attr('x', config.labelWidth - 12)
            .attr('font-size', 11)

        svg.append('text')
            .text('주의')
            .attr('y', config.gridHeight + 19)
            .attr('x', config.labelWidth + (config.gridWidth - config.barMargin) / 2 - 12)
            .attr('font-size', 11)

        svg.append('text')
            .text('경고')
            .attr('y', config.gridHeight + 19)
            .attr('x', config.w - config.barMargin - 12)
            .attr('font-size', 11)

    },
    ring : function(selector, value){
        var chart = new RadialProgressChart(selector, {
            diameter: 70,
            stroke: {
                width: 5
            },
            shadow: {
                width: 0
            },
            max: 100,
            round: false,
            series: [
                {
                    value: 0,
                    color: ['#ff6d00', '#ff6d00']
                }
            ],
            center: function (d) {
                return d.toFixed(1) + ' %'
            }
        });
        chart.update(value);
    }


}